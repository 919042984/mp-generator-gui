mp-generator-gui
==============

### 本工程基于 https://github.com/astarring/mybatis-generator-gui 基础更改,在原项目上简化

## 界面

![img.png](images/img.png)
![img_1.png](images/img_1.png)

## 使用说明

git clone https://gitee.com/919042984/mp-generator-gui.git

打包
![](images/打包.jpg)

成功后在target/jfx/native/目录下有对应的安装文件
![ifx.png](images/ifx.png)
