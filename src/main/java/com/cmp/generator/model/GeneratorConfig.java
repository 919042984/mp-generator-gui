package com.cmp.generator.model;

/**
 * GeneratorConfig is the Config of mybatis generator config exclude database
 * config
 * <p>
 * Created by Owen on 6/16/16.
 */
public class GeneratorConfig {

    /**
     * 本配置的名称
     */
    private String name;
    /**
     *
     */
    private String projectFolder;

    /**
     *
     */
    private String modelPackage;

    /**
     * 表名
     */
    private String tableName;
    /**
     * 作者
     */
    private String author;

    /**
     * 是否开启swagger
     */
    private boolean swagger;
    /**
     * 是否打开输出目录
     */
    private boolean open;

    /**
     * 是否覆盖已有文件
     */
    private boolean fileOverride;

    /**
     * 开启 Kotlin 模式（默认 false）
     */
    private boolean kotlin;


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    public String getProjectFolder() {
        return projectFolder;
    }

    public void setProjectFolder(String projectFolder) {
        this.projectFolder = projectFolder;
    }

    public String getModelPackage() {
        return modelPackage;
    }

    public void setModelPackage(String modelPackage) {
        this.modelPackage = modelPackage;
    }


    public boolean isSwagger() {
        return swagger;
    }

    public void setSwagger(boolean swagger) {
        this.swagger = swagger;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean isFileOverride() {
        return fileOverride;
    }

    public void setFileOverride(boolean fileOverride) {
        this.fileOverride = fileOverride;
    }

    public boolean isKotlin() {
        return kotlin;
    }

    public void setKotlin(boolean kotlin) {
        this.kotlin = kotlin;
    }
}
